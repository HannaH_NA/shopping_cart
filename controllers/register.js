const registerSchema = require("../models/register");
const mongoose = require("mongoose");
const Register = mongoose.model("Register");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const register = {
  register: function (req, res) {
    console.log(req.body);
    var data = { email: "", password: "" };
    data = { ...data, ...req.body };
    bcrypt.hash(data.password, 10, function (error, hash) {
      if (!error) {
        Register.findOne(
          {
            email: data.email
          },
          function (error, existingUser) {
            if (!error) {
              if (existingUser == null) {
                const myData = new Register({
                  ...data,
                  password: hash
                });

                myData
                  .save()
                  .then(response => {
                    const JWTToken = jwt.sign(
                      {
                        _id: response._id,
                      },
                      "secretKey",
                      { expiresIn: "10000d" }
                    );

                    console.log("data save to database" + myData);

                    return res.status(200).json({
                      success: true,
                      data: {
                        _id: response._id,
                        email: response.email,
                        password: response.password,
                        token: JWTToken
                      }
                    });
                  })
                  .catch(error => {
                    return res.status(200).json({
                      success: false,
                      error: {
                        types: "Failure",
                        messages: "failed adding data to db"
                      }
                    });
                  });
              } else {
                return res.status(200).json({
                  success: false,
                  error: {
                    types: "Validation Error",
                    messages: [{ email: "Email Already Exists" }]
                  }
                });
              }
            } else {
              return res.status(200).json({
                success: false,
                error: {
                  types: "Failure",
                  messages: "findOne in db failed"
                }
              });
            }
          }
        );
      } else {
        return res.status(200).json({
          success: false,
          error: {
            types: "Failure",
            messages: "hashing password failed"
          }
        });
      }
    });
  },

  //  login function

  login: function (req, res) {
    var data = { email: "", password: "" };
    data = { ...data, ...req.body };
    Register.findOne(
      {
        email: data.email
      },
      function (error, user) {
        if (!error) {
          if (user != null) {
            bcrypt.compare(data.password, user.password, (err, result) => {
              console.log("error ", err, "result ", result);
              if (!err && result) {
                const JWTToken = jwt.sign(
                  {
                    _id: user._id,
                  },
                  "secretKey",
                  { expiresIn: "10000d" }
                );
                res.cookie("test", JWTToken, {
                  maxAge: 900000,
                  httpOnly: true
                });

                return res.status(200).json({
                  success: true,
                  data: {
                    _id: user._id,
                    email: user.email,
                    token: JWTToken
                  }
                });
              } else {
                res.status(200).json({
                  success: false,
                  error: {
                    message: [{ password: "Invalid password" }]
                  }
                });
              }
            });
          } else {
            return res.status(200).json({
              success: false,
              error: {
                types: "Validation Error",
                messages: [{ email: "Email is not registered" }]
              }
            });
          }
        } else {
          return res.status(200).json({
            success: false,
            error: {
              types: "Failure",
              messages: "findOne in db failed"
            }
          });
        }
      }
    );
  }
};
module.exports = register;
