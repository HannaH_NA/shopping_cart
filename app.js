var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var cons = require("consolidate");
const path = require("path");

var port = 3000;

const site = require("./routes/site");
const api = require("./routes/api");

// Map global promise - get rid of warning
mongoose.Promise = global.Promise;

// view engine setup
app.engine("html", cons.swig);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "html");

// Connect to mongoose
mongoose
  .connect("mongodb://localhost/test", {
    //useMongoClient: true
  })
  .then(() => console.log("MongoDB Connected..."))
  .catch(err => console.log(err));

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/", site);
app.use("/api", api);

app.listen(port, () => {
  console.log("Server listening on port " + port);
});
