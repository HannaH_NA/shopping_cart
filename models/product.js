const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var addProduct = new Schema({
  name: { type: String, require: true },
  price: { type: Number, require: true },
  merchandId: { type: String, required: true },
  createdAt: { type: Date, required: false },
  updatedAt: { type: Number, required: false }
});
addProduct.pre("save", function(next) {
  now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now.getTime();
  next();
});
mongoose.model("Product", addProduct);
