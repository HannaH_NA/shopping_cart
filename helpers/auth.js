const jwt = require("jsonwebtoken");

module.exports = {
  //Check for user authentication

  userAuthCheck: function(req, res, next) {
    if (!req.headers.authorization) {
      return res.status(401).send({ error: "Token missing" });
    }

    var token = req.headers.authorization.split(" ")[1];

    jwt.verify(token, "secretKey", function(err, decoded) {
      if (err)
        return res.status(500).json({
          success: false,
          error: {
            type: "Authentication Failed"
          }
        });

      req.headers.currentUser = decoded;
      next();
    });
  }
};
