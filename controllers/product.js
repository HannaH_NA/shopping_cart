const addProduct = require("../models/product");

const mongoose = require("mongoose");
const Register = mongoose.model("Register");

const Product = mongoose.model("Product");
const product = {
  addProduct: function(req, res) {
    console.log("current user", req.headers.currentUser);
    var data = {
      name: req.body.name,
      price: req.body.price,
      merchandId: req.headers.currentUser._id
    };
    console.log(data);
    const myData = new Product(data);
    myData
      .save()
      .then(item => {
        res.json(item);
        console.log("product save to database" + myData);
      })
      .catch(err => {
        console.log("error" + err);
      });
  },
  listProducts: function(req, res) {
    Product.find({}).then(item => {
      res.json(item);
      console.log(item);
    });
  },
  addToWishList: function(req, res) {
    console.log(req.headers.currentUser);
    console.log("product id", req.body.product_id);
    console.log("user id", req.headers.currentUser._id);
    Register.findOne({
      _id: req.headers.currentUser._id
    }).then(User => {
      Product.findOne({
        _id: req.body.product_id
      }).then(item => {
        User.wishList.push(item);
        User.save().then(result => {
          res.json(result);
          console.log(result);
        });
      });
    });
  },
  wishList: function(req, res) {
    Register.findOne({
      _id: req.headers.currentUser._id
    })
      .populate("wishList")
      .then(item => {
        res.json(item.wishList);
        console.log(item.wishList);
      });
  }
};
module.exports = product;
