const express = require("express");
const router = express.Router();

const authHelper = require("../helpers/auth");

const registerController = require("../controllers/register");
const productController = require("../controllers/product");

router.post("/register", registerController.register);
router.post("/login", registerController.login);
router.post(
  "/addProduct",
  authHelper.userAuthCheck,
  productController.addProduct
);
router.post("/listProducts", productController.listProducts);
router.post(
  "/addToWishList",
  authHelper.userAuthCheck,
  productController.addToWishList
);
router.post("/wishList", authHelper.userAuthCheck, productController.wishList);
module.exports = router;
