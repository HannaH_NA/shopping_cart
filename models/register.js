const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
var registerSchema = new Schema({
  email: { type: String, require: true },
  password: { type: String, require: true },
  wishList: [{ type: Schema.Types.ObjectId, ref: "Product", required: false }],
  createdAt: { type: Date, required: false },
  updatedAt: { type: Number, required: false }
});
registerSchema.pre("save", function (next) {
  now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now.getTime();
  next();
});
mongoose.model("Register", registerSchema);
