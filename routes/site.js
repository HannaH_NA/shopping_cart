const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: true });
const path = require("path");

router.get("/register", urlencodedParser, function(req, res, next) {
  res.sendFile(path.join(__dirname + "/../views/register.html"));
});
router.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/../views/index.html"));
});
router.get("/login", urlencodedParser, function(req, res, next) {
  res.sendFile(path.join(__dirname + "/../views/login.html"));
});
router.get("/addProduct", urlencodedParser, function(req, res, next) {
  res.sendFile(path.join(__dirname + "/../views/product.html"));
});
router.get("/wishList", urlencodedParser, function(req, res, next) {
  res.sendFile(path.join(__dirname + "/../views/wishList.html"));
});
module.exports = router;
